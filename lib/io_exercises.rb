def guessing_game
  number = 1 + rand(100)
  guess = 1000
  guesses = 0

  until guess == number
    puts "Guess a number"
    guess = gets.chomp.to_i
    guesses += 1
    puts guess

    if guess == 0
      puts "too low"
      raise_error(NoMoreInput)
    elsif guess > number
      puts "too high"
    elsif guess < number
      puts "too low"
    end
  end

  puts "you guessed the number #{number} in #{guesses} guesses"
end



def file_shuffler
  puts "gimme somethin to shuffle"
  fileName = gets.chomp
  shuffled_array = File.readlines(fileName).shuffle
  shuffled_file = File.new("{#{fileName}}-shuffled.txt")

  shuffled_array.each do |line|
    File.open(shuffled_file, "a") do |f|
      f.puts line
    end
  end

end
